def c_to_memory location
  stuff = File.read "example_header.h"

  #Capture everything within C braces. This gets rid of
  #most of the C frippery in one go
  stuff = stuff[/\{.*\}/m][1..-1]

  #Remove C comments
  stuff = stuff.scan(/^.*(?=\/\/)/).map(&:strip).join("\n")

  #add a final comma, because every line has one except the last line
  stuff = stuff + ","

  #remove 0xs
  stuff.gsub!("0x", "")

  #remove separators
  stuff.gsub!(", ", "")
  
  #remove trailing commas
  stuff.gsub!(",\n", "\n")
  stuff.gsub!(",", "")
  
  #Split string. Declare victory.
  stuff = stuff.split("\n")

end

def memory_to_vhdl memory, location, vhdl_width
  hex_width = vhdl_width / 4

  data_lines = []
  memory.each do |character_hex|
    character_hex.chars.each_slice(hex_width).each do |vhdl_word|
      line = "    x\"#{vhdl_word.join}\","
      data_lines << line
    end
  end

  num_chars = 256

  result = File.read("template/header1.txt") +
  "  constant font_table_width          : natural := #{vhdl_width};\n" +
  "  constant font_table_words_per_char : natural := #{64/vhdl_width};\n" +
  "  constant font_table_depth          : natural := #{(64*num_chars)/vhdl_width};\n" +

  File.read("template/header2.txt") +
  (data_lines.join("\n")[0..-2] + "\n") + 
  File.read("template/footer.txt")
  
  #puts result
  File.write location, result
end

def go c_location, vhdl_location, vhdl_width
  memory = c_to_memory c_location
  memory_to_vhdl memory, vhdl_location, vhdl_width
end

 go "example_header.h", "out.vhd", 64