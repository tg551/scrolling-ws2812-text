quietly set PROJECT_DIR "//fs/share/xilinx/projects/ws2812_driver_onoff"

file delete -force presynth 
vlib presynth

vmap presynth presynth

vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/fpga_constants.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/character_ram.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/circular_queue.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/debounce.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/rising_edge_detector.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/input_conditioning.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/font_table_pkg.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/font_rom.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/output_data_slider.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/uart_rx.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/uart_tx.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/uart_controller.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/ws_2812_driver.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/swirly_canvas_rom.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/canvas_rom.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/canvas_painter.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/top.vhd"

vcom -explicit -2008 -work presynth "${PROJECT_DIR}/tb/uart_functions.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/tb/top_tb.vhd"

vsim -L presynth  -t 1ps presynth.tb 

add wave -group clkrst           -label clk                                    /tb/uut/clk
add wave -group clkrst           -label reset                                  /tb/uut/reset

add wave -group uart -label pi2fpga  /tb/uut/pi_to_fpga_pin
add wave -group uart -label new_data_rx /tb/uut/new_data_rxed

add wave -group circbuff -label read_addr  -radix unsigned /tb/uut/circular_queue_1/read_address
add wave -group circbuff -label write_addr -radix unsigned /tb/uut/circular_queue_1/write_address
add wave -group circbuff -label next_char  /tb/next_char_to_print

add wave -group slider -label request_next /tb/uut/output_data_slider_1/request_next_queue_output
add wave -group slider -label run_scroll /tb/uut/output_data_slider_1/run_scroll
add wave -group slider -label scroll_timer /tb/uut/output_data_slider_1/scroll_timer
add wave -group slider -label scroll_counter /tb/uut/output_data_slider_1/scroll_counter
add wave -group slider -label scroll_counter_done /tb/uut/output_data_slider_1/scroll_counter_done
add wave -group slider -label state /tb/uut/output_data_slider_1/state

add wave -group canvas -label canv_counter  -radix unsigned    /tb/uut/canvas_painter_1/canvas_counter
add wave -group canvas -label read_Addr     -radix unsigned    /tb/uut/canvas_painter_1/read_Address
add wave -group canvas -label colour        -radix hexadecimal /tb/uut/driver/colour
add wave -group canvas -label led_index     -radix unsigned    /tb/uut/driver/led_index

add wave -group drv  -label bit_index -radix unsigned /tb/uut/driver/bit_index
add wave -group drv -label output /tb/uut/driver/output


#change constants
change sim:/tb/uut/canvas_painter_1/canvas_update_clocks 10000
change sim:/tb/uut/output_data_slider_1/clocks_per_scroll 25000

force -freeze sim:/tb/uut/driver/led_data x\"FFFFFFFFFFFFFF\" 0

run 50 ms;
wave zoom full
