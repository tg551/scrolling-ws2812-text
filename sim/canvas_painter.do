quietly set PROJECT_DIR "//fs/share/xilinx/projects/ws2812_driver_onoff"

file delete -force presynth 
vlib presynth

vmap presynth presynth

vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/swirly_canvas_rom.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/fpga_constants.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/canvas_ram.vhd"
vcom -explicit -2008 -work presynth "${PROJECT_DIR}/hdl/canvas_painter.vhd"

vcom -explicit -2008 -work presynth "${PROJECT_DIR}/tb/canvas_painter_tb.vhd"

vsim -L presynth  -t 1ps presynth.tb 

add wave -group clkrst           -label clk                                    /tb/uut/clk
add wave -group clkrst           -label reset                                  /tb/uut/reset
add wave -radix unsigned -label addr sim:/tb/uut/canvas_ram_1/read_address

add wave -radix unsigned                            /tb/uut/canvas_counter
add wave -radix unsigned                            /tb/uut/canvas_counter_timer
add wave -radix unsigned         -label memword_cnt /tb/uut/memory_word_counter
add wave                         -label canvas      /tb/uut/canvas
add wave -radix unsigned         -label top         /tb/top
add wave -radix unsigned         -label bot         /tb/bottom

run 4 ms;
wave zoom full
