library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.math_real.all;

package uart_functions is
  procedure uart_send(data         : in  std_logic_vector(7 downto 0);
                      baud_rate    : in  integer;
                      start        : in  time;
                      current_time : in  time;
                      uart_value   : out std_logic;
                      done         : out boolean
                      );
end package;

package body uart_functions is
  procedure uart_send(data         : in  std_logic_vector(7 downto 0);
                      baud_rate    : in  integer;
                      start        : in  time;
                      current_time : in  time;
                      uart_value   : out std_logic;
                      done : out boolean) is
    variable current_bit                   : integer;
    variable bit_time                      : time;
    variable bits_elapsed_since_start      : integer;

  begin
    done     := false;
    bit_time := 1 sec / baud_rate;

    bits_elapsed_since_start := (current_time - start) / bit_time;

    if bits_elapsed_since_start = 0 then
      uart_value := '0';                      -- start bit
      return;
    elsif bits_elapsed_since_start <= 8 then  --send the data lsb first
      uart_value := data(7-(bits_elapsed_since_start - 1));
      return;
    elsif bits_elapsed_since_start = 9 then   -- stop bit
      uart_value := '1';
      return;
    else                                      -- we finished!
      uart_value := '1';
      done       := true;
      return;
    end if;
  end uart_send;
end package body;
