
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;
use work.uart_functions.all;

use work.fpga_constants.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity tb is
end tb;

architecture behavior of tb is

  -- Component Declaration for the Unit Under Test (UUT)

  component actual_top
    port(
      clk            : in  std_logic;
      reset_n        : in  std_logic;
      data_out       : out std_logic;
      pi_to_fpga_pin : in  std_logic;
      fpga_to_pi_pin : out std_logic;
      pb_0           : in  std_logic;
      pb_1           : in  std_logic;
      led_0          : out std_logic;
      led_1          : out std_logic
      );
  end component;


  --Inputs
  signal clk            : std_logic := '0';
  signal reset_n        : std_logic := '0';
  signal pi_to_fpga_pin : std_logic := '0';
  signal pb_0           : std_logic := '0';
  signal pb_1           : std_logic := '0';

  --Outputs
  signal data_out       : std_logic;
  signal fpga_to_pi_pin : std_logic;
  signal led_0          : std_logic;
  signal led_1          : std_logic;

  -- Clock period definitions
  constant clk_period : time := 20 ns;

  signal next_char_to_print : character;

  function reverse_any_vector (a : in std_logic_vector)
    return std_logic_vector is
    variable result : std_logic_vector(a'range);
    alias aa        : std_logic_vector(a'reverse_range) is a;
  begin
    for i in aa'range loop
      result(i) := aa(i);
    end loop;
    return result;
  end;  -- function reverse_any_vector

  function to_slv(the_char : in character) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(character'pos(the_char), 8));
  end to_slv;

  function to_char(the_slv : in std_logic_vector) return character is
  begin
    return character'val(to_integer(unsigned(reverse_any_vector(the_slv))));
  end to_char;
  
begin


  -- Instantiate the Unit Under Test (UUT)
  uut : actual_top port map (
    clk            => clk,
    reset_n        => reset_n,
    data_out       => data_out,
    pi_to_fpga_pin => pi_to_fpga_pin,
    fpga_to_pi_pin => fpga_to_pi_pin,
    pb_0           => pb_0,
    pb_1           => pb_1,
    led_0          => led_0,
    led_1          => led_1
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  update_next_char : process
    variable the_ram          : ram_type;
    variable read_address     : unsigned(integer(ceil(log2(real(ram_depth)))) - 1 downto 0);
    variable read_address_int : integer;

  begin
    wait until rising_edge(clk);
    --if << signal uut.empty : std_logic >> = '1' then
    --  next_char_to_print <= '{';
    --else
      the_ram          := << signal uut.circular_queue_1.ram_1.ram         : ram_type >>;
      read_address     := << signal uut.circular_queue_1.read_address : unsigned >>;
      read_address_int := to_integer(read_address);


      next_char_to_print <= to_char(the_ram(read_address_int));
    --end if;
  end process;



  -- Stimulus process
  stim_proc : process

    --output file
    file log        : text is out "log.txt";
    variable logbuf : line;

    procedure send_byte(data : in std_logic_vector) is
      variable start      : time;
      variable done       : boolean;
      variable next_value : std_logic;
    begin
      start := now;
      loop
        wait until rising_edge(clk);
        uart_send(data, 9600, start, now, next_value, done);
        pi_to_fpga_pin <= next_value;
        exit when done;
      end loop;
    end send_byte;

    procedure send_character(the_character : in character) is
    begin
      send_byte(to_slv(the_character));
    end send_character;

    procedure send_message(message : in string) is
    begin
      for i in message'range loop
        send_character(message(i));
      end loop;
    end send_message;

    function std_logic_image_without_tick(val : in std_logic) return string is
    begin
      return character'image(std_logic'image(val)(2));
    end std_logic_image_without_tick;

    --prints the contents of the circular queue
    procedure print_circular_queue is
      variable the_ram       : ram_type;
      variable read_address  : unsigned(integer(ceil(log2(real(ram_depth)))) - 1 downto 0);
      variable write_address : unsigned(integer(ceil(log2(real(ram_depth)))) - 1 downto 0);

      variable current_char          : std_logic_vector(7 downto 0);
      variable current_char_unsigned : unsigned(7 downto 0);
      variable current_char_int      : integer;
    begin
      
      the_ram       := << signal uut.circular_queue_1.ram_1.ram           : ram_type >>;
      read_address  := << signal uut.circular_queue_1.read_address   : unsigned >>;
      write_address := << signal uut.circular_queue_1.write_address : unsigned >>;

      for i in to_integer(read_address) to to_integer(write_address) loop
        write(logbuf, to_char(the_ram(i)));
        for j in the_ram(i)'range loop
        --write(logbuf, std_logic_image_without_tick(the_ram(i)(j)));
        end loop;
      end loop;
    end print_circular_queue;

    procedure reset_proc is
    begin
      reset_n        <= '0';
      pi_to_fpga_pin <= '1';
      pb_0           <= '0';
      pb_1           <= '0';

      wait for 5 * clk_period;
      reset_n <= '1';
      wait until rising_edge(clk);
    end reset_proc;
    
  begin
    -- hold reset state for 100 ns.
    reset_proc;

    send_message("abcdef");
    print_circular_queue;

    wait for 20 ms;

    send_message("g");

    writeline(log, logbuf);
    wait;
  end process;
end;
