
library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity tb is
end tb;

architecture behavior of tb is

  -- Component Declaration for the Unit Under Test (UUT)
  
  component canvas_painter
    port(
      clk    : in  std_logic;
      reset  : in  std_logic;
      canvas : out std_logic_vector(1535 downto 0)
      );
  end component;


  --Inputs
  signal clk   : std_logic := '0';
  signal reset : std_logic := '0';

  --Outputs
  signal canvas : std_logic_vector(1535 downto 0);

  signal top    : integer;
  signal bottom : integer;

  -- Clock period definitions
  constant clk_period : time := 20 ns;
  
begin

  -- Instantiate the Unit Under Test (UUT)
  uut : canvas_painter port map (
    clk    => clk,
    reset  => reset,
    canvas => canvas
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  top_bot : process
    variable memory_word_counter : integer;
  begin
    wait until rising_edge(clk);
    memory_word_counter := << signal uut.memory_word_counter : integer range 0 to 48 >>;

    top    <= (memory_word_counter * 24) + 31;
    bottom <= memory_word_counter * 24;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    wait for 100 ns;

    wait for clk_period*10;

    -- insert stimulus here 

    wait;
  end process;

end;
