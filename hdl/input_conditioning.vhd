library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.fpga_constants.all;

entity input_conditioning is
  port(clk   : in STD_LOGIC;
       reset : in STD_LOGIC;
       pb_0  : in STD_LOGIC;
       pb_1  : in STD_LOGIC;

       pb_0_strobe : out STD_LOGIC;
       pb_1_strobe : out STD_LOGIC);
end input_conditioning;

architecture Behavioral of input_conditioning is
  signal pb_0_debounced : STD_LOGIC;
  signal pb_1_debounced : STD_LOGIC;
begin
    pb_0_db : entity work.debounce
    generic map (debounce_time => 10 ms)
    port map (
      clk        => clk,
      reset      => reset,
      in_signal  => pb_0,
      out_signal => pb_0_debounced);
    
  pb_0_re : entity work.rising_edge_detector
    port map (
      clk         => clk,
      reset       => reset,
      input       => pb_0_debounced,
      edge_strobe => pb_0_strobe);  

  pb_1_db : entity work.debounce
    generic map (debounce_time => 10 ms)
    port map (
      clk        => clk,
      reset      => reset,
      in_signal  => pb_1,
      out_signal => pb_1_debounced);

  pb_1_re : entity work.rising_edge_detector
    port map (
      clk         => clk,
      reset       => reset,
      input       => pb_1_debounced,
      edge_strobe => pb_1_strobe);

end Behavioral;

