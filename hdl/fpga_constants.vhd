
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package fpga_constants is
----------------------------------------------------------------------------------------------
-- Define Main CLK specification
----------------------------------------------------------------------------------------------
  constant fpga_clock_freq : positive := 50_000_000;

----------------------------------------------------------------------------------------------
-- Communications definitions
----------------------------------------------------------------------------------------------  
  constant baud_rate : natural := 115_200;

----------------------------------------------------------------------------------------------
-- Light thing definitions
----------------------------------------------------------------------------------------------    
  constant number_of_leds : positive := 64;
  constant scroll_time    : time     := 50 ms;
--  constant scroll_time    : time     := 600 us;

----------------------------------------------------------------------------------------------
-- character buffer ram definitions.
----------------------------------------------------------------------------------------------
  constant ram_width : integer := 8;
  constant ram_depth : integer := 1023;
  type ram_type is array (0 to ram_depth) of std_logic_vector (ram_width - 1 downto 0);
  
end package;

package body fpga_constants is          -- can this be deleted?
end package body;
