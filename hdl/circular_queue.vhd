library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.fpga_constants.all;


entity circular_queue is
  port (clk   : in std_logic;
        reset : in std_logic;

        enqueue       : in  std_logic;
        dequeue       : in  std_logic;
        write_in_data : in  std_logic_vector(7 downto 0);
        read_out_data : out std_logic_vector(7 downto 0);
        empty         : out std_logic;
        full          : out std_logic
        );
end circular_queue;

architecture rtl of circular_queue is
  constant ram_depth_addr_length : integer := integer(ceil(log2(real(ram_depth))));

  --ram signals
  signal write_enable  : std_logic;     --the memory location where the *next* write will occur
                                        --to
  signal read_address  : unsigned(ram_depth_addr_length - 1 downto 0);  -- the memory location
                                                                        -- where the *next* read
                                                                        -- will occur from.
  signal write_address : unsigned(ram_depth_addr_length - 1 downto 0);
  signal data_in       : std_logic_vector(ram_width downto 0);
  signal data_out      : std_logic_vector(ram_width downto 0);
  signal empty_int     : std_logic;
  signal full_int      : std_logic;
begin
  ram_1 : entity work.character_ram
    port map (
      clk           => clk,
      read_address  => read_address,
      write_address => write_address,
      write_enable  => write_enable,
      write_in      => write_in_data,

      read_out => read_out_data);

  empty_int <= '1' when read_address = write_address
               else '0';
  empty <= empty_int;
  

  full_int <= '1' when write_address = ram_depth
              else '0';
  
  full <= full_int;

  write_enable <= '1' when full_int = '0' and enqueue = '1'
                  else '0';

  --allow a 'read' to be performed
  dequeue_something : process (clk) is
    procedure advance_read_address is
    begin
      if read_address >= write_address - 1 then
        read_address <= to_unsigned(0, read_address'length);
      else
        read_address <= read_address + 1;
      end if;
    end advance_read_address;

  begin
    if rising_edge(clk) then
      if reset = '1' then
        read_address <= to_unsigned(0, read_address'length);
      elsif dequeue = '1' and empty_int = '0' then
        advance_read_address;
      end if;
    end if;
  end process dequeue_something;

--Allow a write to be performed, except when there is no more room
  update_write_address : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        write_address <= to_unsigned(0, write_address'length);
      else
        if enqueue = '1' and full_int = '0' then
          write_address <= write_address + 1;
        end if;
      end if;
    end if;
  end process update_write_address;
end rtl;
