library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
use ieee.math_real.all;
use work.font_package.all;

entity font_rom is
  port(clk : in std_logic;

       read_address : in  integer range 0 to font_table_depth;
       read_out     : out std_logic_vector(font_table_width - 1 downto 0)
       );

end font_rom;

architecture rtl of font_rom is

  constant font_table_rom                : font_table_t := font_table_data;
  attribute ram_style                    : string;
  attribute ram_style of font_table_rom  : constant is "block";
begin
  ram_proc : process (clk) is
  begin
    if rising_edge(clk) then
      read_out <= font_table_rom(read_address);
    end if;
  end process ram_proc;
end rtl;

