library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
use ieee.math_real.all;
use work.fpga_constants.all;

entity character_ram is
  port(clk   : in std_logic;

       read_address  : in  unsigned(integer(ceil(log2(real(ram_depth)))) -1 downto 0);
       write_address : in  unsigned(integer(ceil(log2(real(ram_depth)))) -1 downto 0);
       write_enable  : in  std_logic;
       write_in      : in  std_logic_vector(ram_width -1 downto 0);
       read_out      : out std_logic_vector(ram_width -1 downto 0)
       );

end character_ram;
--attempt to synthesize a dual port block ram
architecture rtl of character_ram is

  signal ram : ram_type := (others => (others => '0'));  -- remove simulation warnings.. we
                                                         -- don't actually need to zero this
begin
  ram_proc : process (clk) is
  begin
    if rising_edge(clk) then
      if write_enable = '1' then
        ram(to_integer(write_address)) <= write_in;
      end if;

      read_out <= ram(to_integer(read_address));
    end if;
  end process ram_proc;
end rtl;

