library IEEE;

use IEEE.std_logic_1164.all;

use work.fpga_constants.all;

entity debounce is
  generic (
    debounce_time : TIME
    );
  port (
    clk        : in  STD_LOGIC;
    reset      : in  STD_LOGIC;
    in_signal  : in  STD_LOGIC;
    out_signal : out STD_LOGIC
    );
end debounce;


architecture rtl of debounce is
  constant clock_cycle_time : TIME    := 1 sec / fpga_clock_freq;
  constant counter_limit    : INTEGER := debounce_time / clock_cycle_time;
  signal counter            : INTEGER range 0 to counter_limit;
  signal in_signal_d1       : STD_LOGIC;
begin
  go : process(clk, reset)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        counter      <= 0;
        in_signal_d1 <= '0';
        out_signal   <= '0';
      else
        in_signal_d1 <= in_signal;
        if (in_signal xor in_signal_d1) = '1' then
          counter <= 0;
        elsif counter < counter_limit then
          counter <= counter + 1;
        else
          out_signal <= in_signal;
        end if;
      end if;
    end if;
  end process;
end architecture;
