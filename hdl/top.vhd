library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.font_package.all;
use work.fpga_constants.all;

entity actual_top is
  port(
    clk      : in  std_logic;
    reset_n  : in  std_logic;
    data_out : out std_logic;

    pi_to_fpga_pin : in  std_logic;
    fpga_to_pi_pin : out std_logic;

    pb_0  : in  std_logic;
    pb_1  : in  std_logic;
    led_0 : out std_logic;
    led_1 : out std_logic
    );
end actual_top;

architecture behavioral of actual_top is
  signal reset : std_logic;

  constant clock_time : time := 1 sec / fpga_clock_freq;


  signal colour    : std_logic_vector(23 downto 0);
  signal led_index : integer range number_of_leds - 1 downto 0;
  signal led_data  : std_logic_vector(number_of_leds - 1 downto 0);


  --uart signals
  signal framing_error   : std_logic;
  signal pi_to_fpga_data : std_logic_vector(7 downto 0);

  signal send                  : std_logic;
  signal new_data_rxed         : std_logic;
  signal tx_ready              : std_logic;
  signal circular_queue_output : std_logic_vector (7 downto 0);
  signal ready                 : std_logic;

  --queue signals
  signal enqueue  : std_logic;
  signal dequeue  : std_logic;
  signal read_out : std_logic_vector(7 downto 0);
  signal empty    : std_logic;
  signal full     : std_logic;

  --slider signals
  signal request_next_queue_output : std_logic;

  --brighten/darken! (Remember to always apply this late in the canvas pipeline!)
  signal brighten : std_logic;
  signal darken   : std_logic;
begin

  reset <= '1' when reset_n = '0' else
           '0';

  input_conditioning_1 : entity work.input_conditioning
    port map (
      clk         => clk,
      reset       => reset,
      pb_0        => pb_0,
      pb_1        => pb_1,
      pb_0_strobe => brighten,
      pb_1_strobe => darken);

  uart_controller_1 : entity work.uart_controller
    port map (
      clk             => clk,
      reset           => reset,
      pi_to_fpga_pin  => pi_to_fpga_pin,
      fpga_to_pi_data => circular_queue_output,
      send            => send,

      new_data_rxed   => new_data_rxed,
      tx_ready        => tx_ready,
      fpga_to_pi_pin  => fpga_to_pi_pin,
      pi_to_fpga_data => pi_to_fpga_data,
      framing_error   => open);


  driver : entity work.ws2812_driver_onoff
    generic map (
      number_of_leds => number_of_leds)
    port map (
      clk       => clk,
      reset     => reset,
      colour    => colour,
      led_index => led_index,
      led_data  => led_data,
      output    => data_out);

  circular_queue_1 : entity work.circular_queue
    port map (
      clk           => clk,
      reset         => reset,
      enqueue       => new_data_rxed,
      dequeue       => dequeue,
      write_in_data => pi_to_fpga_data,
      read_out_data => circular_queue_output,
      empty         => empty,
      full          => full
      );

  led_0 <= full;

  output_data_slider_1 : entity work.output_data_slider
    port map (
      clk                       => clk,
      reset                     => reset,
      circular_queue_output     => circular_queue_output,
      empty                     => empty,
      request_next_queue_output => request_next_queue_output,
      led_data                  => led_data,
      led_1                     => led_1);

  output_data : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        dequeue <= '0';
        send    <= '0';
      else
        if request_next_queue_output = '1' then
          dequeue <= '1';
          send    <= '1';
        else
          dequeue <= '0';
          send    <= '0';
        end if;
      end if;
    end if;
  end process output_data;

  canvas_painter_1 : entity work.canvas_painter
    port map (
      clk       => clk,
      reset     => reset,
      led_index => led_index,
      brighten  => brighten,
      darken    => darken,
      colour    => colour
      );



end Behavioral;

