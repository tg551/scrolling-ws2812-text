library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;
use ieee.math_real.all;
use work.swirly_colour_package.all;

entity canvas_rom is
  port(clk : in std_logic;

       --read_address : in  unsigned(integer(ceil(log2(real(number_of_swirls)))) -1 downto 0);
       read_address : in  integer range number_of_swirls - 1 downto 0;
       read_out     : out std_logic_vector(size_of_swirl -1 downto 0)
       );

end canvas_rom;
--attempt to synthesize a dual port block ram
architecture rtl of canvas_rom is

  constant swirly_rom               : swirly_colour_t := swirly_table;
  attribute ram_style               : string;
  attribute ram_style of swirly_rom : constant is "block";
begin
  ram_proc : process (clk) is
  begin
    if rising_edge(clk) then
      read_out <= swirly_rom(read_address);
    end if;
  end process ram_proc;
end rtl;

