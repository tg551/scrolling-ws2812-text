--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: rising_edge_detector.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::ProASIC3L> <Die::M1A3P1000L> <Package::484 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity rising_edge_detector is
  port (
    clk         : in  STD_LOGIC;
    reset       : in  STD_LOGIC;
    input       : in  STD_LOGIC;
    edge_strobe : out STD_LOGIC
    );
end rising_edge_detector;
architecture rtl of rising_edge_detector is
  signal input_d1 : STD_LOGIC;
begin
  assign_d1 : process(clk, reset)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        input_d1 <= '0';
      else
        input_d1 <= input;
      end if;
    end if;
  end process;

  edge_strobe <= input and not input_d1;
end architecture;
