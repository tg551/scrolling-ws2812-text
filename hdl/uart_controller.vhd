library IEEE;
use IEEE.STD_LOGIC_1164.all;
entity uart_controller is
  port(clk             : in std_logic;
       reset           : in std_logic;
       pi_to_fpga_pin  : in std_logic;
       fpga_to_pi_data : in std_logic_vector(7 downto 0);
       send            : in std_logic;

       new_data_rxed   : out std_logic;
       tx_ready        : out std_logic;
       fpga_to_pi_pin  : out std_logic;
       pi_to_fpga_data : out std_logic_vector(7 downto 0);
       framing_error   : out std_logic
       );
end uart_controller;
architecture Behavioral of uart_controller is
  signal new_data      : std_logic;
begin
  uart_rx_1 : entity work.uart_rx
    port map (
      clk           => clk,
      reset         => reset,
      uart_rx       => pi_to_fpga_pin,
      new_data      => new_data_rxed,
      framing_error => framing_error,
      data          => pi_to_fpga_data);

  uart_tx_1 : entity work.uart_tx
    port map (
      clk     => clk,
      reset   => reset,
      send    => send,
      data    => fpga_to_pi_data,
      ready   => tx_ready,
      uart_tx => fpga_to_pi_pin);
end Behavioral;

