library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.math_real.all;
use work.swirly_colour_package.all;
use work.fpga_constants.all;

entity canvas_painter is
  port(clk       : in  std_logic;
       reset     : in  std_logic;
       led_index : in  integer range number_of_leds - 1 downto 0;
       brighten  : in  std_logic;
       darken    : in  std_logic;
       colour    : out std_logic_vector(23 downto 0));
end canvas_painter;

architecture rtl of canvas_painter is
  constant memory_words_per_canvas : integer := 1536 / size_of_swirl;

  constant canvas_update_time : time := 12 ms;

  constant clock_time           : time    := 1 sec / fpga_clock_freq;
  constant canvas_update_clocks : integer := canvas_update_time / clock_time;

  constant number_of_canvasses : integer := (number_of_swirls * size_of_swirl) / 1536;


  signal canvas_counter            : integer range 0 to number_of_canvasses - 1;
  signal canvas_counter_timer      : integer range 0 to canvas_update_clocks - 1;
  signal canvas_counter_timer_done : std_logic;
  signal new_canvas_counter        : std_logic;

  signal colour_int : std_logic_vector(23 downto 0);

  signal read_address : integer range number_of_swirls - 1 downto 0;

  constant max_darkness : integer := 7;
  signal darkness       : integer range 0 to max_darkness;

  
begin

  canvas_ram_1 : entity work.canvas_rom
    port map (
      clk          => clk,
      read_address => read_address,
      read_out     => colour_int);

  read_address <= (canvas_counter * memory_words_per_canvas) + led_index;

  update_colour : process (darkness, colour_int) is
    impure function shift_left_by_darkness(input  : in std_logic_vector;
                                            amount : in integer)
      return std_logic_vector is
    begin
      return std_logic_vector(shift_left(unsigned(input), amount));
    end shift_left_by_darkness;
    
    variable colour_int_1, colour_int_2, colour_int_3 : std_logic_vector(7 downto 0);
  begin
    colour_int_1 := colour_int(23 downto 16);
    colour_int_2 := colour_int(15 downto 8);
    colour_int_3 := colour_int(7 downto 0);

    colour(23 downto 16) <= shift_left_by_darkness(colour_int_1, darkness);
    colour(15 downto 8)  <= shift_left_by_darkness(colour_int_2, darkness);
    colour(7 downto 0)   <= shift_left_by_darkness(colour_int_3, darkness);
  end process update_colour;


  canvas_counter_timer_update : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        null;
      --canvas_counter_timer      <= 0;
      --canvas_counter_timer_done <= '0';
      else
        if canvas_counter_timer >= canvas_update_clocks - 1then
          canvas_counter_timer <= 0;
        canvas_counter_timer_done <= '1';
      else
        canvas_counter_timer      <= canvas_counter_timer + 1;
        canvas_counter_timer_done <= '0';
      end if;
    end if;
  end if;
end process canvas_counter_timer_update;

canvas_counter_update : process (clk) is
begin
  if rising_edge(clk) then
    if reset = '1' then
      canvas_counter     <= 0;
      new_canvas_counter <= '0';
    else
      if canvas_counter_timer_done = '1' then
        if canvas_counter >= number_of_canvasses - 1 then
          canvas_counter     <= 0;
          new_canvas_counter <= '1';
        else
          canvas_counter     <= canvas_counter + 1;
          new_canvas_counter <= '1';
        end if;
      else
        new_canvas_counter <= '0';
      end if;
    end if;
  end if;
end process canvas_counter_update;

--Implementing a darkness select.
--This design assumes that S6 has barrel shifters, and can rotate a 8 bit SLV by an arbitrary
--amount.
update_darkness_reg : process (clk) is
begin
  if rising_edge(clk) then
    if reset = '1' then
      darkness <= 0;
    else
      if brighten = '1' and darkness < max_darkness then
        darkness <= darkness + 1;
      elsif darken = '1' and darkness > 0 then
        darkness <= darkness - 1;
      end if;
    end if;
  end if;
end process update_darkness_reg;

end rtl;

