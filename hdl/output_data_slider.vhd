library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;

use work.fpga_constants.all;
use work.font_package.all;

entity output_data_slider is
  port(clk                       : in  std_logic;
       reset                     : in  std_logic;
       circular_queue_output     : in  std_logic_vector(7 downto 0);
       empty                     : in  std_logic;
       request_next_queue_output : out std_logic;
       led_data                  : out std_logic_vector(number_of_leds - 1 downto 0);
       led_1                     : out std_logic
       );
end output_data_slider;

architecture rtl of output_data_slider is
  type state_t is (idle, wait_read_rom, shifting_a_character, done);
  signal state : state_t;

  signal character_holder : std_logic_vector((number_of_leds * 2) - 1 downto 0);


  --timing
  constant clock_time        : time     := 1 sec / fpga_clock_freq;
  constant clocks_per_scroll : positive := scroll_time / clock_time;  -- this is our counter limit.
  signal scroll_timer        : integer range 0 to clocks_per_scroll;
  signal scroll_timer_done   : std_logic;

  constant scroll_counter_max : integer := 7;
  signal scroll_counter       : integer range 0 to scroll_counter_max;
  signal scroll_counter_done  : std_logic;

  signal run_scroll : std_logic;

  signal read_address : integer range 0 to font_table_depth;
  signal font_rom_out : std_logic_vector(font_table_width - 1 downto 0);
begin

  led_1 <= '1' when scroll_counter >= 1 else '0';

  led_data <= character_holder((number_of_leds * 2) - 1 downto number_of_leds);

  read_address <= to_integer(unsigned(circular_queue_output));


  font_rom_1 : entity work.font_rom
    port map (
      clk          => clk,
      read_address => read_address,
      read_out     => font_rom_out
      );


  fsm_nextstate : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        state <= idle;
      else
        case state is
          when idle =>
            if empty = '0' then
              state <= wait_read_rom;
            end if;

          when wait_read_rom =>
            state <= shifting_a_character;
            
          when shifting_a_character =>
            if scroll_counter_done = '1' then
              state <= done;
            end if;

          when done =>
            state <= idle;

          when others =>
            state <= idle;
        end case;
      end if;
    end if;
  end process fsm_nextstate;

  fsm_outputs : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        character_holder          <= (others => '0');
        run_scroll                <= '0';
        request_next_queue_output <= '0';
      else
        case state is
          when idle =>
            run_scroll                <= '0';
            if empty = '0' then
              request_next_queue_output <= '1';
            end if;

          when wait_read_rom =>
            request_next_queue_output                     <= '0';
            character_holder(number_of_leds - 1 downto 0) <= font_rom_out;
            
          when shifting_a_character =>
            request_next_queue_output <= '0';
            run_scroll                <= '1';
            if scroll_timer_done = '1' then
              character_holder(character_holder'length - 1 downto 8) <=
                character_holder(character_holder'length - 9 downto 0);
            end if;

          when done =>
            run_scroll <= '0';

          when others =>
            null;
        end case;
      end if;
    end if;
  end process fsm_outputs;

  scroll_timer_proc : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        scroll_timer      <= 0;
        scroll_timer_done <= '0';
      else
        if run_scroll = '1' then
          if scroll_timer >= clocks_per_scroll then
            scroll_timer      <= 0;
            scroll_timer_done <= '1';
          else
            scroll_timer      <= scroll_timer + 1;
            scroll_timer_done <= '0';
          end if;
        else
          scroll_timer      <= 0;
          scroll_timer_done <= '0';
        end if;
      end if;
    end if;
  end process scroll_timer_proc;

  scroll_counter_proc : process (clk) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        scroll_counter      <= 0;
        scroll_counter_done <= '0';
      else
        if run_scroll = '1' then
          if scroll_timer_done = '1' then
            if scroll_counter >= scroll_counter_max then
              scroll_counter      <= 0;
              scroll_counter_done <= '1';
            else
              scroll_counter      <= scroll_counter + 1;
              scroll_counter_done <= '0';
            end if;
          end if;
        else
          scroll_counter      <= 0;
          scroll_counter_done <= '0';
        end if;
      end if;
    end if;
  end process scroll_counter_proc;
end rtl;
