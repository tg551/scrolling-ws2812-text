library IEEE;
use IEEE.NUMERIC_STD.all;
use ieee.std_logic_1164.all;

use work.fpga_constants.all;

entity ws2812_driver_onoff is
  generic(
    number_of_leds : positive
    );


  port(
    clk       : in  std_logic;
    reset     : in  std_logic;
    led_data  : in  std_logic_vector(number_of_leds - 1 downto 0);
    led_index : out natural range 0 to number_of_leds - 1;
    colour    : in  std_logic_vector(23 downto 0);
    output    : out std_logic
    );
end ws2812_driver_onoff;

architecture rtl of ws2812_driver_onoff is

  -- Timing constants
  constant t0h : time := 350 ns;
  constant t1h : time := 700 ns;

  constant t0l : time := 800 ns;
  constant t1l : time := 600 ns;

  constant res : time := 51 us;

  -- clockified constants
  constant time_per_clock : time := 1 sec / fpga_clock_freq;

  constant t0h_clocks : positive := t0h / time_per_clock;
  constant t1h_clocks : positive := t1h / time_per_clock;
  constant t0l_clocks : positive := t0l / time_per_clock;
  constant t1l_clocks : positive := t1l / time_per_clock;
  constant res_clocks : positive := res / time_per_clock;

  --timer
  signal timer       : natural range 0 to res_clocks;
  signal timer_limit : natural range 0 to res_clocks;
  signal timer_done  : std_logic;

  -- input array indexer
  signal next_input        : std_logic;
  signal current_led_value : std_logic;

  signal led_index_int : natural range 0 to number_of_leds - 1;

  -- single light bit counter
  signal bit_index         : integer range 0 to 23;
  signal current_bit_value : std_logic;

  -- statey things.
  type driver_state is (high, low, wait_res);
  signal state : driver_state;

  --provide useful symbolic names for some things
  impure function current_bit_is_final_bit_in_led return boolean is
  begin
    return bit_index = 23;
  end current_bit_is_final_bit_in_led;

  impure function current_led_is_final_led return boolean is
  begin
    return led_index_int = number_of_leds - 1;
  end current_led_is_final_led;
begin

  led_index <= led_index_int;

  -- Design Decision: We could either work out which LED is the right LED by using a mux or by
  -- shifting. Currently using a mux (probably? I didn't actually check this)
  current_led_value <= led_data(led_index_int);

  --colour            <= canvas((led_index_int*24) + 23 downto led_index_int*24);
  current_bit_value <= colour(bit_index);

  fsm : process (clk, reset) is
    procedure dispatch_next is
    begin

      -- if the bitcounter is equal to 23, then we haven finished outputting
      -- everything we need to output for the current LED.. so we need to dispatch as
      -- appropriate for the next bit.
      if current_bit_is_final_bit_in_led then
        if current_led_is_final_led then
          state <= wait_res;
        else
          led_index_int <= led_index_int + 1;
          bit_index     <= 0;
          state         <= high;
        end if;
      else
        bit_index <= bit_index + 1;
        state     <= high;
      end if;
    end dispatch_next;
    
  begin
    if rising_edge(clk) then
      if reset = '1' then
        state <= wait_res;
      else

        case state is
          when wait_res =>
            bit_index     <= 0;
            led_index_int <= 0;
            if timer_done = '1' then
              state <= high;
            end if;
            
          when high =>
            if timer_done = '1' then
              state <= low;
            end if;
            
          when low =>
            if timer_done = '1' then
              dispatch_next;
            end if;
        end case;
      end if;
    end if;
  end process fsm;

  counter_proc : process (clk, reset) is
  begin
    if rising_edge(clk) then
      if reset = '1' then
        timer      <= 0;
        timer_done <= '0';
      else
        if timer >= timer_limit then
          timer      <= 0;
          timer_done <= '1';
        else
          timer      <= timer + 1;
          timer_done <= '0';
        end if;
      end if;
    end if;
  end process counter_proc;

  limit_select : process (state, current_led_value, current_bit_value) is
  begin
    if state = wait_res then
      timer_limit <= res_clocks;
    elsif current_led_value = '1' then
      if state = high and current_bit_value = '1' then
        timer_limit <= t1h_clocks;
      elsif state = high and current_bit_value = '0' then
        timer_limit <= t0h_clocks;
      elsif state = low and current_bit_value = '1' then
        timer_limit <= t1l_clocks;
      else
        timer_limit <= t0l_clocks;
      end if;
    else
      -- turn the LED off, so unconditionally emit zeros
      if state = high then
        timer_limit <= t0h_clocks;
      else
        timer_limit <= t0l_clocks;
      end if;
    end if;
  end process limit_select;

  output_select : process (clk, reset) is
  begin
    if rising_edge(clk) then            --register output to prevent glitching.
      if reset = '1' then
        output <= '0';
      else
        case state is
          when wait_res | low =>
            output <= '0';
          when high =>
            output <= '1';
        end case;
      end if;
    end if;
  end process output_select;

end rtl;
